## About this repository

This project will be used for the [Youtube's series "My Java SE Tech
Stack"](https://www.youtube.com/playlist?list=PLt35-TchTYnGfa54kE5_KIenM8IoGXSKC).
With it we'll learn concepts around a set of tools and technologies that were
put together in the
[blog](https://gitlab.com/EPadronU/my-journey-to-the-core/-/blob/master/software-development/java-se-technology-stack.md),
as well as in a [series of articles in
LinkedIn](https://www.linkedin.com/pulse/my-java-se-technology-stack-edinson-e-padr%C3%B3n-urdaneta/).

package com.gitlab.epadronu.shoppingcart;

import java.util.Collection;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public interface CanContainProducts<P extends Product> {

  @NotNull
  Set<@Valid P> getProducts();

  void setProducts(@NotNull Collection<@Valid P> products);

  @PositiveOrZero
  long getAmount(@NotNull @Valid P product);

  void setAmount(@NotNull @Valid P product, @Positive long amount);

  @PositiveOrZero
  long increaseAmount(@NotNull @Valid P product);

  @PositiveOrZero
  long increaseAmount(@NotNull @Valid P product, @Positive long amount);

  @PositiveOrZero
  long decreaseAmount(@NotNull @Valid P product);

  @PositiveOrZero
  long decreaseAmount(@NotNull @Valid P product, @Positive long amount);
}

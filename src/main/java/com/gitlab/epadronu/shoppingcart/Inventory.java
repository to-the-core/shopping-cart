package com.gitlab.epadronu.shoppingcart;

import java.math.BigDecimal;
import javax.validation.constraints.PositiveOrZero;

public interface Inventory extends CanContainProducts<StockableProduct> {

  @PositiveOrZero
  BigDecimal getTotalCostValue();

  @PositiveOrZero
  BigDecimal getTotalPriceValue();
}

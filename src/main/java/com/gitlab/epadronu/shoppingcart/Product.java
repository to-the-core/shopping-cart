package com.gitlab.epadronu.shoppingcart;

import javax.validation.constraints.NotEmpty;

public interface Product {

  long getReference();

  @NotEmpty
  String getBrand();

  @NotEmpty
  String getName();

  @NotEmpty
  String getDescription();
}

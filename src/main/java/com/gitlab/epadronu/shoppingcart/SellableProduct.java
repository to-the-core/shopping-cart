package com.gitlab.epadronu.shoppingcart;

import java.math.BigDecimal;
import javax.validation.constraints.PositiveOrZero;

public interface SellableProduct extends Product {

  @PositiveOrZero
  BigDecimal getPrice();
}

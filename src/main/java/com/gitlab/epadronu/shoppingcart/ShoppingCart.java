package com.gitlab.epadronu.shoppingcart;

import java.math.BigDecimal;
import javax.validation.constraints.PositiveOrZero;

public interface ShoppingCart extends CanContainProducts<SellableProduct> {

  @PositiveOrZero
  BigDecimal getTotalValue();
}

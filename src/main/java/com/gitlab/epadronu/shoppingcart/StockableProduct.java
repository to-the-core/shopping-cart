package com.gitlab.epadronu.shoppingcart;

import java.math.BigDecimal;
import javax.validation.constraints.PositiveOrZero;

import com.gitlab.epadronu.shoppingcart.validations.PriceIsGreaterThanOrEqualsToCost;

@PriceIsGreaterThanOrEqualsToCost
public interface StockableProduct extends SellableProduct {

  @PositiveOrZero
  BigDecimal getCost();
}

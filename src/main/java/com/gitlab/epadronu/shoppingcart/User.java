package com.gitlab.epadronu.shoppingcart;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {

  private static final AtomicLong idProvider = new AtomicLong();

  private final long id;

  private final String name;

  private final ShoppingCart shoppingCart;

  public User(
    @Min(value = 1L, message = "The id must be greater or equals to {value}") final long id,
    @NotEmpty @Size(min = 2, max = 30) final String name,
    @NotNull final ShoppingCart shoppingCart) {
    this.id = id;
    this.name = name;
    this.shoppingCart = shoppingCart;
  }

  public User(
    @NotEmpty @Size(min = 2, max = 30) final String name,
    @NotNull final ShoppingCart shoppingCart) {
    this(idProvider.incrementAndGet(), name, shoppingCart);
  }

  public long getId() {
    return id;
  }

  @NotEmpty
  public String getName() {
    return name;
  }

  @NotNull
  public ShoppingCart getShoppingCart() {
    return shoppingCart;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getName(), getShoppingCart());
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final User user = (User) o;
    return getId() == user.getId() &&
           getName().equals(user.getName()) &&
           getShoppingCart().equals(user.getShoppingCart());
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
      .add("id=" + id)
      .add("name='" + name + "'")
      .add("shoppingCart=" + shoppingCart)
      .toString();
  }
}

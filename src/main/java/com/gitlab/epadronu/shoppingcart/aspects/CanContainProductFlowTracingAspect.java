package com.gitlab.epadronu.shoppingcart.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public final class CanContainProductFlowTracingAspect extends FlowTracingAspect {

  @Override
  @Pointcut("initialization(com.gitlab.epadronu.shoppingcart.CanContainProducts+.new(..))")
  protected void constructors() {
  }

  @Override
  @Pointcut("execution(* com.gitlab.epadronu.shoppingcart.CanContainProducts+.*(..))")
  protected void methods() {
  }
}

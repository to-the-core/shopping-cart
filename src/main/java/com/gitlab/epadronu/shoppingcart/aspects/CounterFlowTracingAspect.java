package com.gitlab.epadronu.shoppingcart.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public final class CounterFlowTracingAspect extends FlowTracingAspect {

  @Override
  @Pointcut("initialization(com.gitlab.epadronu.shoppingcart.impl.Counter.new(..))")
  protected void constructors() {
  }

  @Override
  @Pointcut("execution(* com.gitlab.epadronu.shoppingcart.impl.Counter.*(..))")
  protected void methods() {
  }
}

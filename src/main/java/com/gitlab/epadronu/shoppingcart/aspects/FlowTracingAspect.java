package com.gitlab.epadronu.shoppingcart.aspects;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.EntryMessage;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
abstract public class FlowTracingAspect {

  private static final Logger logger = LogManager.getLogger();

  private final ThreadLocal<EntryMessage> message = new ThreadLocal<>();

  @Pointcut
  protected abstract void constructors();

  @Pointcut
  protected abstract void methods();

  @Before("methods() || constructors()")
  public void beforeMethodsOrConstructors(final JoinPoint thisJoinPoint) {
    message.set(
      logger.traceEntry(
        "`{}.{}` with {}",
        thisJoinPoint.getStaticPart().getSignature().getDeclaringType().getSimpleName(),
        thisJoinPoint.getStaticPart().getSignature().getName(),
        Arrays.toString(thisJoinPoint.getArgs())));
  }

  @AfterReturning(pointcut = "methods()", returning = "returnedValue")
  public void afterReturningMethods(final Object returnedValue) {
    logger.traceExit(message.get(), returnedValue);

    message.remove();
  }

  @AfterReturning("constructors() && this(instance)")
  public void afterReturningConstructors(final Object instance) {
    logger.traceExit(message.get(), instance);

    message.remove();
  }
}

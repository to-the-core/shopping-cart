package com.gitlab.epadronu.shoppingcart.aspects;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.ConstructorSignature;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class ValidationsAspect {

  private static final Logger logger = LogManager.getLogger();

  private static final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

  private static final Validator validator = factory.getValidator();

  private static final ExecutableValidator executableValidator = validator.forExecutables();

  private static String constraintViolationsPrettyPrinted(
    final Set<ConstraintViolation<Object>> constraintViolations) {
    return constraintViolations
      .stream()
      .map(it -> String.format("%s %s", it.getPropertyPath(), it.getMessage()))
      .collect(Collectors.joining(", ", "[", "]"));
  }

  @Pointcut(
    "withincode(String Object+.toString())"
    + " || withincode(int Object+.hashCode())"
    + " || withincode(boolean Object+.equals(Object))")
  private void withinObjectMethods() {
  }

  @Pointcut(
    "within("
    + "  com.gitlab.epadronu.shoppingcart.validations..*"
    + "  || com.gitlab.epadronu.shoppingcart.aspects..*)")
  private void withinIgnoredPackages() {
  }

  @Pointcut(
    "call(("
    + "  com.gitlab.epadronu.shoppingcart.User"
    + "  || com.gitlab.epadronu.shoppingcart.impl..*).new(..))")
  public void constructors() {
  }

  @Pointcut(
    "target(instance)"
    + " && call(* com.gitlab.epadronu.shoppingcart..*.*(..))"
    + " && !withinObjectMethods()"
    + " && !withinIgnoredPackages()")
  public void methods(final Object instance) {
  }

  @Before("constructors()")
  public void beforeConstructors(
    JoinPoint thisJoinPoint,
    JoinPoint.StaticPart thisJoinPointStaticPart) {
    final Constructor<?> constructor =
      ((ConstructorSignature) thisJoinPointStaticPart.getSignature()).getConstructor();

    final Set<ConstraintViolation<Object>> constraintViolations = executableValidator
      .validateConstructorParameters(constructor, thisJoinPoint.getArgs());

    if (!constraintViolations.isEmpty()) {
      logger.info(
        "The constructor invocation of `{}` has the following violations `{}`",
        thisJoinPointStaticPart.getSignature().getDeclaringType().getSimpleName(),
        constraintViolationsPrettyPrinted(constraintViolations));
    }
  }

  @AfterReturning(pointcut = "constructors()", returning = "instance")
  public void afterReturningConstructors(
    JoinPoint.StaticPart thisJoinPointStaticPart,
    Object instance) {
    final Set<ConstraintViolation<Object>> constraintViolations = validator.validate(instance);

    if (!constraintViolations.isEmpty()) {
      logger.info(
        "The new created instance of `{}` has the following violations `{}`",
        thisJoinPointStaticPart.getSignature().getDeclaringType().getSimpleName(),
        constraintViolationsPrettyPrinted(constraintViolations));
    }
  }

  @Before("methods(instance)")
  public void beforeMethods(
    JoinPoint thisJoinPoint,
    JoinPoint.StaticPart thisJoinPointStaticPart,
    Object instance) {
    final Class<?> targetClass = thisJoinPointStaticPart.getSignature().getDeclaringType();

    final String methodName = thisJoinPointStaticPart.getSignature().getName();

    final Method method = ((MethodSignature) thisJoinPointStaticPart.getSignature()).getMethod();

    final Set<ConstraintViolation<Object>> constraintViolations = executableValidator
      .validateParameters(instance, method, thisJoinPoint.getArgs());

    if (!constraintViolations.isEmpty()) {
      logger.info(
        "The method `{}` invocation of `{}` has the following violations `{}`",
        methodName,
        targetClass.getSimpleName(),
        constraintViolationsPrettyPrinted(constraintViolations));
    }
  }

  @AfterReturning(pointcut = "methods(instance)", returning = "returnedValue")
  public void afterReturningMethods(
    JoinPoint.StaticPart thisJoinPointStaticPart,
    Object instance,
    Object returnedValue) {
    final Class<?> targetClass = thisJoinPointStaticPart.getSignature().getDeclaringType();

    final String methodName = thisJoinPointStaticPart.getSignature().getName();

    final Method method = ((MethodSignature) thisJoinPointStaticPart.getSignature()).getMethod();

    final Set<ConstraintViolation<Object>> constraintViolations = executableValidator
      .validateReturnValue(instance, method, returnedValue);

    if (!constraintViolations.isEmpty()) {
      logger.info(
        "The method `{}` of `{}` that returned `{}` has the following violations `{}`",
        methodName,
        targetClass.getSimpleName(),
        returnedValue,
        constraintViolationsPrettyPrinted(constraintViolations));
    }
  }
}

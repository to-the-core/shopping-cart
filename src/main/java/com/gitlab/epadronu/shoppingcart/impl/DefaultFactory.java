package com.gitlab.epadronu.shoppingcart.impl;

import java.math.BigDecimal;

import com.gitlab.epadronu.shoppingcart.Inventory;
import com.gitlab.epadronu.shoppingcart.SellableProduct;
import com.gitlab.epadronu.shoppingcart.ShoppingCart;
import com.gitlab.epadronu.shoppingcart.StockableProduct;

public abstract class DefaultFactory {

  public static ShoppingCart makeShoppingCart() {
    return new ShoppingCartImpl();
  }

  public static Inventory makeInventory() {
    return new InventoryImpl();
  }

  public static SellableProduct makeSellableProduct(
    final String brand,
    final String name,
    final String description,
    final BigDecimal price) {
    return new SellableProductImpl(brand, name, description, price);
  }

  public static StockableProduct makeStockableProduct(
    final String brand,
    final String name,
    final String description,
    final BigDecimal cost,
    final BigDecimal price) {
    return new StockableProductImpl(brand, name, description, cost, price);
  }
}

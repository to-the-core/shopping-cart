package com.gitlab.epadronu.shoppingcart.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import com.gitlab.epadronu.shoppingcart.Inventory;
import com.gitlab.epadronu.shoppingcart.StockableProduct;

class InventoryImpl implements Inventory {

  private final Counter<StockableProduct> stock = new Counter<>();

  @Override
  public Set<StockableProduct> getProducts() {
    return stock.getItems();
  }

  @Override
  public void setProducts(final Collection<StockableProduct> products) {
    stock.setItems(products);
  }

  @Override
  public long getAmount(final StockableProduct product) {
    return stock.getCount(product);
  }

  @Override
  public void setAmount(final StockableProduct product, final long amount) {
    stock.setCount(product, amount);
  }

  @Override
  public long increaseAmount(final StockableProduct product) {
    return stock.increaseCount(product);
  }

  @Override
  public long increaseAmount(final StockableProduct product, final long amount) {
    return stock.increaseCount(product, amount);
  }

  @Override
  public long decreaseAmount(final StockableProduct product) {
    return stock.decreaseCount(product);
  }

  @Override
  public long decreaseAmount(final StockableProduct product, final long amount) {
    return stock.decreaseCount(product, amount);
  }

  @Override
  public BigDecimal getTotalCostValue() {
    return stock.getItems()
                .stream()
                .map(product -> product
                  .getCost()
                  .multiply(BigDecimal.valueOf(stock.getCount(product))))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
  }

  @Override
  public BigDecimal getTotalPriceValue() {
    return stock.getItems()
                .stream()
                .map(product -> product
                  .getPrice()
                  .multiply(BigDecimal.valueOf(stock.getCount(product))))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
  }
}

package com.gitlab.epadronu.shoppingcart.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import com.gitlab.epadronu.shoppingcart.SellableProduct;
import com.gitlab.epadronu.shoppingcart.ShoppingCart;

class ShoppingCartImpl implements ShoppingCart {

  private final Counter<SellableProduct> content = new Counter<>();

  @Override
  public BigDecimal getTotalValue() {
    return content.getItems()
                  .stream()
                  .map(product -> product
                    .getPrice()
                    .multiply(BigDecimal.valueOf(content.getCount(product))))
                  .reduce(BigDecimal::add)
                  .orElse(BigDecimal.ZERO);
  }

  @Override
  public Set<SellableProduct> getProducts() {
    return content.getItems();
  }

  @Override
  public void setProducts(final Collection<SellableProduct> products) {
    content.setItems(products);
  }

  @Override
  public long getAmount(final SellableProduct product) {
    return content.getCount(product);
  }

  @Override
  public void setAmount(final SellableProduct product, final long amount) {
    content.setCount(product, amount);
  }

  @Override
  public long increaseAmount(final SellableProduct product) {
    return content.increaseCount(product);
  }

  @Override
  public long increaseAmount(final SellableProduct product, final long amount) {
    return content.increaseCount(product, amount);
  }

  @Override
  public long decreaseAmount(final SellableProduct product) {
    return content.decreaseCount(product);
  }

  @Override
  public long decreaseAmount(final SellableProduct product, final long amount) {
    return content.decreaseCount(product, amount);
  }
}

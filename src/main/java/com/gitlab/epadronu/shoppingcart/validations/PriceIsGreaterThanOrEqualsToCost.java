package com.gitlab.epadronu.shoppingcart.validations;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import com.gitlab.epadronu.shoppingcart.validations.validators.PriceIsGreaterThanOrEqualsToCostValidator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, TYPE_USE})
@Retention(RUNTIME)
@Constraint(validatedBy = PriceIsGreaterThanOrEqualsToCostValidator.class)
@Documented
@Repeatable(PriceIsGreaterThanOrEqualsToCost.List.class)
public @interface PriceIsGreaterThanOrEqualsToCost {

  String message() default (
    "{com.gitlab.epadronu.shoppingcart.PriceIsGreaterThanOrEqualsToCost.message}");

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

  @Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
  @Retention(RUNTIME)
  @Documented
  @interface List {

    PriceIsGreaterThanOrEqualsToCost[] value();
  }
}


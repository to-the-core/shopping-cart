package com.gitlab.epadronu.shoppingcart.validations.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.gitlab.epadronu.shoppingcart.StockableProduct;
import com.gitlab.epadronu.shoppingcart.validations.PriceIsGreaterThanOrEqualsToCost;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

public class PriceIsGreaterThanOrEqualsToCostValidator
  implements ConstraintValidator<PriceIsGreaterThanOrEqualsToCost, StockableProduct> {

  @Override
  public void initialize(final PriceIsGreaterThanOrEqualsToCost constraintAnnotation) {
  }

  @Override
  public boolean isValid(
    final StockableProduct stockableProduct,
    final ConstraintValidatorContext constraintValidatorContext) {
    boolean isValid = stockableProduct.getPrice().compareTo(stockableProduct.getCost()) >= 0;

    if (!isValid) {
      HibernateConstraintValidatorContext hibernateConstraintValidatorContext =
        constraintValidatorContext
          .unwrap(HibernateConstraintValidatorContext.class);

      hibernateConstraintValidatorContext.disableDefaultConstraintViolation();

      hibernateConstraintValidatorContext
        .addMessageParameter("name", stockableProduct.getName())
        .addMessageParameter("price", stockableProduct.getPrice())
        .addMessageParameter("cost", stockableProduct.getCost())
        .buildConstraintViolationWithTemplate(
          "{com.gitlab.epadronu.shoppingcart.PriceIsGreaterThanOrEqualsToCost.message}")
        .addConstraintViolation();
    }

    return isValid;
  }
}

package com.gitlab.epadronu.shoppingcart;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gitlab.epadronu.shoppingcart.impl.DefaultFactory;
import com.gitlab.epadronu.shoppingcart.utils.AllureExtension;
import com.gitlab.epadronu.shoppingcart.utils.PascalCaseDisplayNameGenerator;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

@DisplayNameGeneration(PascalCaseDisplayNameGenerator.class)
@ExtendWith({SoftAssertionsExtension.class, AllureExtension.class})
@Tag("Smoke")
@Severity(SeverityLevel.NORMAL)
public class AppTest extends BaseTest {

  private Inventory inventory;

  private List<User> users;

  public static Stream<Arguments> settingTheInventoryShouldPutInItTheRightProducts() {
    return Stream.of(
      Arguments.of(
        List.of(mochaPhone, mahoganyPhone, mahoganyPhone, mochaPhone, mochaPhone),
        Set.of(mochaPhone, mahoganyPhone)),
      Arguments.of(
        List.of(mahoganyLaptop, mochaLaptop, mahoganyExtendedGuarantee, mahoganyLaptop),
        Set.of(mahoganyLaptop, mochaLaptop, mahoganyExtendedGuarantee)));
  }

  @BeforeEach
  void loadTestData() {
    final TestData testData = getTestData();

    inventory = testData.getInventory();
    users = testData.getUsers();
  }

  @Test
  @Severity(SeverityLevel.BLOCKER)
  public void basicSanityTest(final SoftAssertions softly) {
    softly.assertThat(inventory)
          .extracting(Inventory::getProducts)
          .isEqualTo(Set.of(
            mochaPhone,
            mochaLaptop,
            mochaTablet,
            mahoganyPhone,
            mahoganyExtendedGuarantee,
            mahoganyLaptop));

    softly.assertThat(users)
          .extracting(User::getName)
          .containsOnly("Samantha", "Bob", "Jessica", "Lucas");
  }

  @ParameterizedTest(name = "[{index}] Setting {0} products should thrown an error")
  @ValueSource(ints = {0, -1, -10, -100, -1_000_000})
  @Tag("Sad-path")
  public void settingProductsToLessThanOneShouldThrownAnException(int amount) {
    assertThatIllegalArgumentException()
      .isThrownBy(() -> inventory.setAmount(mochaPhone, amount))
      .withMessage("Count must be greater or equals to 1");
  }

  @ParameterizedTest(name = "[{index}] Putting {0} into the inventory means it should have {1}")
  @MethodSource
  public void settingTheInventoryShouldPutInItTheRightProducts(
    final Collection<StockableProduct> productsToBeIncludedInInventory,
    final Set<StockableProduct> productsInInventory) {
    inventory.setProducts(productsToBeIncludedInInventory);

    assertThat(inventory)
      .extracting(Inventory::getProducts)
      .usingRecursiveComparison()
      .isEqualTo(productsInInventory);
  }

  @Test
  public void theInventoryCountShouldBeCorrect() {
    final Map<StockableProduct, Long> amounts = inventory
      .getProducts()
      .stream()
      .collect(Collectors.toMap(
        Function.identity(),
        inventory::getAmount));

    assertThat(amounts)
      .containsEntry(mochaPhone, 4L)
      .containsEntry(mochaLaptop, 1L)
      .containsEntry(mochaTablet, 4L)
      .containsEntry(mahoganyPhone, 1L)
      .containsEntry(mahoganyLaptop, 1L)
      .containsEntry(mahoganyExtendedGuarantee, 2L);
  }

  @Test
  public void settingOneProductShouldSucceed() {
    assertThatCode(() -> inventory.setAmount(mochaPhone, 1)).doesNotThrowAnyException();
  }

  @Test
  public void decreasingTheAmountOfAKnownProductToZeroShouldSucceed() {
    assertThat(inventory.decreaseAmount(mahoganyExtendedGuarantee, 2L))
      .isEqualTo(0L);
  }

  @Test
  public void increasingTheAmountOfAKnownProductShouldSucceed() {
    assertThat(inventory.increaseAmount(mahoganyExtendedGuarantee))
      .isEqualTo(3L);
  }

  @Test
  public void increasingTheAmountOfAKnownProductByLessThanOneShouldFail() {
    assertThatIllegalArgumentException()
      .isThrownBy(() -> inventory.increaseAmount(mochaLaptop, -1L));
  }

  @Test
  public void decreasingTheAmountOfAKnownProductShouldSucceed() {
    assertThat(inventory.decreaseAmount(mahoganyLaptop))
      .isEqualTo(0L);
  }

  @Test
  public void decreasingTheAmountOfAKnownProductBeyondTheCurrentAmountShouldFail() {
    assertThatIllegalArgumentException()
      .isThrownBy(() -> inventory.decreaseAmount(mochaLaptop, 10));
  }

  @Test
  public void decreasingTheAmountOfAKnownProductByLessThanOneShouldFail() {
    assertThatIllegalArgumentException()
      .isThrownBy(() -> inventory.decreaseAmount(mochaLaptop, -1L));
  }

  @Test
  public void decreasingTheAmountOfAUnknownProductShouldFail() {
    final StockableProduct unknown = DefaultFactory.makeStockableProduct(
      "Unknown",
      "Unknown",
      "Unknown",
      BigDecimal.ZERO,
      BigDecimal.ZERO);

    assertThatIllegalArgumentException()
      .isThrownBy(() -> inventory.decreaseAmount(unknown, 1L));
  }
}

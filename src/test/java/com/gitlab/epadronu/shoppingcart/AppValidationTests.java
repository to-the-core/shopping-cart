package com.gitlab.epadronu.shoppingcart;

import java.math.BigDecimal;
import java.util.Collections;

import com.gitlab.epadronu.shoppingcart.impl.DefaultFactory;
import com.gitlab.epadronu.shoppingcart.utils.AllureExtension;
import com.gitlab.epadronu.shoppingcart.utils.PascalCaseDisplayNameGenerator;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.gitlab.epadronu.shoppingcart.impl.DefaultFactory.makeStockableProduct;

@ExtendWith({AllureExtension.class})
@DisplayNameGeneration(PascalCaseDisplayNameGenerator.class)
public class AppValidationTests {

  @Test
  public void shouldExerciseTheValidationsAspect() {
    makeStockableProduct(
      "System42",
      "Lion Laptop",
      "A linux-powered workstation",
      BigDecimal.valueOf(999.99),
      BigDecimal.valueOf(1299.99));

    makeStockableProduct(
      "",
      "",
      "",
      BigDecimal.valueOf(-1L),
      BigDecimal.valueOf(-1L))
      .getName();

    makeStockableProduct(
      "System42",
      "Lion Laptop",
      "A linux-powered workstation",
      BigDecimal.valueOf(1299.99),
      BigDecimal.valueOf(999.99));

    new User(0L, "X", null);

    DefaultFactory.makeInventory()
                  .setProducts(
                    Collections.singletonList(
                      makeStockableProduct(
                        "Mapple",
                        "Phone",
                        "I don't know Rick, it looks invalid",
                        BigDecimal.valueOf(-2999.99),
                        BigDecimal.valueOf(4000.00))));
  }
}
